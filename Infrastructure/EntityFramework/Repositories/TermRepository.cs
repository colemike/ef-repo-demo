﻿using System.Threading.Tasks;
using Core.Models;
using Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.EntityFramework.Repositories
{
    public class TermRepository : ITermRepository
    {
        public async Task AddAsync(Term term)
        {
            using (var context = new EntityFrameworkContext())
            {
                context.Entry(term).State = EntityState.Added;
                await context.SaveChangesAsync();
            }
        }
    }
}