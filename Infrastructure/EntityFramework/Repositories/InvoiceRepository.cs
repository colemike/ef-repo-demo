﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Enums;
using Core.Models;
using Core.Repositories;
using Core.Repositories.Includes;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.EntityFramework.Repositories
{
    public class InvoiceRepository : IInvoiceRepository
    {
        public async Task<Invoice> GetByIdAsync(Guid id, InvoiceIncludes? includes)
        {
            using (var context = new EntityFrameworkContext())
            {
                return await InvoicesQuery(context, includes.Value).FirstOrDefaultAsync(i => i.InvoiceId == id);
            }
        }

        public async Task<List<Invoice>> GetByStatus(InvoiceStatuses status, InvoiceIncludes? includes)
        {
            using (var context = new EntityFrameworkContext())
            {
                return await InvoicesQuery(context, includes.Value).Where(i => i.Status == status).ToListAsync();
            }
        }

        public async Task AddAsync(Invoice invoice)
        {
            using (var context = new EntityFrameworkContext())
            {
                context.Entry(invoice).State = EntityState.Added;
                await context.SaveChangesAsync();
            }
        }

        private IQueryable<Invoice> InvoicesQuery(EntityFrameworkContext context, InvoiceIncludes includes)
        {
            var query = context.Invoices.AsQueryable();

            if (includes.HasFlag(InvoiceIncludes.Client))
            {
                query = query.Include(invoice => invoice.Client);
            }

            if (includes.HasFlag(InvoiceIncludes.Term))
            {
                query = query.Include(invoice => invoice.Term);
            }

            return query;
        }
    }
}