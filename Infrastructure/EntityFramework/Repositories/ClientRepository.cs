﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Models;
using Core.Repositories;
using Core.Repositories.Includes;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.EntityFramework.Repositories
{
    public class ClientRepository : IClientRepository
    {
        public async Task<List<Client>> GetAllAsync(ClientIncludes? includes = ClientIncludes.None)
        {
            using (var context = new EntityFrameworkContext())
            {
                return await ClientQuery(context, includes.Value).ToListAsync();
            }
        }

        public async Task AddAsync(Client client)
        {
            using (var context = new EntityFrameworkContext())
            {
                context.Entry(client).State = EntityState.Added;
                await context.SaveChangesAsync();
            }
        }

        private IQueryable<Client> ClientQuery(EntityFrameworkContext context, ClientIncludes includes)
        {
            var query = context.Clients.AsQueryable();

            if (includes.HasFlag(ClientIncludes.MainContact))
            {
                query = query.Include(client => client.MainContact);
            }

            return query;
        }
    }
}