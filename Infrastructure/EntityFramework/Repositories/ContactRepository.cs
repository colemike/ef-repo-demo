﻿using System.Threading.Tasks;
using Core.Models;
using Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.EntityFramework.Repositories
{
    public class ContactRepository : IContactRepository
    {
        public async Task AddAsync(Contact contact)
        {
            using (var context = new EntityFrameworkContext())
            {
                context.Entry(contact).State = EntityState.Added;
                await context.SaveChangesAsync();
            }
        }
    }
}