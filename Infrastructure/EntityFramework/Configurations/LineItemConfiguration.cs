﻿using Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityFramework.Configurations
{
    public class LineItemConfiguration : IEntityTypeConfiguration<LineItem>
    {
        public void Configure(EntityTypeBuilder<LineItem> builder)
        {
            builder.HasKey(i => i.LineItemId);

            builder.HasOne(i => i.Invoice);

            builder.HasOne(i => i.ItemType);

            builder.Property(i => i.Description).HasMaxLength(200);
        }
    }
}