﻿using Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityFramework.Configurations
{
    public class ContactConfiguration : IEntityTypeConfiguration<Contact>
    {
        public void Configure(EntityTypeBuilder<Contact> builder)
        {
            builder.HasKey(i => i.ContactId);

            builder.Property(i => i.FirstName).IsRequired().HasMaxLength(50);

            builder.Property(i => i.LastName).IsRequired().HasMaxLength(50);

            builder.Property(i => i.EmailAddress).IsRequired().HasMaxLength(100);
        }
    }
}