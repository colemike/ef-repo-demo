﻿using System;
using System.Collections.Generic;

namespace Core.Models
{
    public class Client
    {
        public Guid ClientId { get; set; }
        public string CompanyName { get; set; }
        public Guid MainContactId { get; set; }
        public Contact MainContact { get; set; }
    }
}