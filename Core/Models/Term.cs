﻿using System;

namespace Core.Models
{
    public class Term
    {
        public Guid TermId { get; set; }
        public string Name { get; set; }
        public int DueInDays { get; set; }
    }
}