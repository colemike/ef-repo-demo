﻿using System;

namespace Core.Models
{
    public class LineItem
    {
        public Guid LineItemId { get; set; }
        public Guid InvoiceId { get; set; }
        public Invoice Invoice { get; set; }
        public Guid ItemTypeId { get; set; }
        public ItemType ItemType { get; set; }
        public string Description { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitPrice { get; set; }

        //Don't do stuff like this here. Create Extension Method instead.
        //public decimal Amount => Quantity * UnitPrice;
    }

    public static class LineItemExtensions
    {
        public static decimal GetAmount(this LineItem lineItem)
        {
            return lineItem.Quantity * lineItem.UnitPrice;
        }
    }
}