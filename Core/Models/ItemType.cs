﻿using System;

namespace Core.Models
{
    public class ItemType
    {
        public Guid ItemTypeId { get; set; }
        public string Name { get; set; }
    }
}