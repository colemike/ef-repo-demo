﻿using System;
using Core.Enums;

namespace Core.Models
{
    public class Invoice
    {
        public Guid InvoiceId { get; set; }
        public string Subject { get; set; }
        public DateTime IssueDate { get; set; }
        public Guid ClientId { get; set; }
        public Client Client { get; set; }
        public Currencies Currency { get; set; }
        public Guid TermId { get; set; }
        public Term Term { get; set; }
        public InvoiceStatuses Status { get; set; }
    }
}
