﻿namespace Core.Enums
{
    public enum InvoiceStatuses
    {
        Open,
        Deleted,
        Paid
    }
}