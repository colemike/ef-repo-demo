﻿using System;

namespace Core.Repositories.Includes
{
    [Flags]
    public enum ClientIncludes
    {
        None,
        MainContact
    }
}