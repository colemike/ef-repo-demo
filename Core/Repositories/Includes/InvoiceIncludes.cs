﻿using System;

namespace Core.Repositories.Includes
{
    [Flags]
    public enum InvoiceIncludes
    {
        None,
        Client,
        Term
    }
}