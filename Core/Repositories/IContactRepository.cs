﻿using System.Threading.Tasks;
using Core.Models;

namespace Core.Repositories
{
    public interface IContactRepository
    {
        Task AddAsync(Contact term);
    }
}