﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Models;
using Core.Repositories.Includes;

namespace Core.Repositories
{
    public interface IClientRepository
    {
        Task<List<Client>> GetAllAsync(ClientIncludes? clientIncludes);
        Task AddAsync(Client client);
    }
}