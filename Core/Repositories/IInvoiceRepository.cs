﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Enums;
using Core.Models;
using Core.Repositories.Includes;

namespace Core.Repositories
{
    public interface IInvoiceRepository
    {
        Task<Invoice> GetByIdAsync(Guid id, InvoiceIncludes? includes);
        Task<List<Invoice>> GetByStatus(InvoiceStatuses status, InvoiceIncludes? includes);
        Task AddAsync(Invoice invoice);
    }
}