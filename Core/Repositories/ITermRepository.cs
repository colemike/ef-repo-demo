﻿using System.Threading.Tasks;
using Core.Models;

namespace Core.Repositories
{
    public interface ITermRepository
    {
        Task AddAsync(Term term);
    }
}