﻿using System;
using System.Threading.Tasks;
using Core.Enums;
using Core.Models;
using Core.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ef_repo_demo.Controllers
{
    [Route("api/seed")]
    [ApiController]
    public class SeedController : ControllerBase
    {
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly ITermRepository _termRepository;
        private readonly IContactRepository _contactRepository;
        private readonly IClientRepository _clientRepository;

        public SeedController(IInvoiceRepository invoiceRepository, ITermRepository termRepository, IContactRepository contactRepository, IClientRepository clientRepository)
        {
            _invoiceRepository = invoiceRepository;
            _termRepository = termRepository;
            _contactRepository = contactRepository;
            _clientRepository = clientRepository;
        }

        public async Task SeedAsync()
        {
            var term = new Term
            {
                TermId = Guid.NewGuid(),
                DueInDays = 30,
                Name = "Net 30"
            };

            await _termRepository.AddAsync(term);

            var contact = new Contact
            {
                ContactId = Guid.NewGuid(),
                EmailAddress = "George@Constanza.com",
                FirstName = "George",
                LastName = "Costanza"
            };

            await _contactRepository.AddAsync(contact);

            var client = new Client
            {
                ClientId = Guid.NewGuid(),
                CompanyName = "Vandalay Industries",
                MainContactId = contact.ContactId
            };

            await _clientRepository.AddAsync(client);

            var invoice = new Invoice
            {
                InvoiceId = Guid.NewGuid(),
                ClientId = client.ClientId,
                Currency = Currencies.USD,
                IssueDate = DateTime.UtcNow,
                Status = InvoiceStatuses.Open,
                Subject = "Latex Exporting",
                TermId = term.TermId
            };

            await _invoiceRepository.AddAsync(invoice);
        }
    }
}