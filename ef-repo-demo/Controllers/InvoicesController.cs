﻿using System;
using System.Threading.Tasks;
using Core.Models;
using Core.Repositories;
using Core.Repositories.Includes;
using Microsoft.AspNetCore.Mvc;

namespace ef_repo_demo.Controllers
{
    [Route("api/invoices")]
    [ApiController]
    public class InvoicesController : ControllerBase
    {
        private readonly IInvoiceRepository _invoiceRepository;

        public InvoicesController(IInvoiceRepository invoiceRepository)
        {
            _invoiceRepository = invoiceRepository;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Invoice>> GetAsync(Guid id)
        {
            return await _invoiceRepository.GetByIdAsync(id, InvoiceIncludes.Client | InvoiceIncludes.Term);
        }
    }
}