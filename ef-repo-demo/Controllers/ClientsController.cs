﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Models;
using Core.Repositories;
using Core.Repositories.Includes;
using Microsoft.AspNetCore.Mvc;

namespace ef_repo_demo.Controllers
{
    [Route("api/clients")]
    [ApiController]
    public class ClientsController : ControllerBase
    {
        private readonly IClientRepository _clientRepository;

        public ClientsController(IClientRepository clientRepository)
        {
            _clientRepository = clientRepository;
        }

        [HttpGet]
        public async Task<ActionResult<List<Client>>> GetAsync()
        {
            return await _clientRepository.GetAllAsync(ClientIncludes.MainContact);
        }
    }
}
